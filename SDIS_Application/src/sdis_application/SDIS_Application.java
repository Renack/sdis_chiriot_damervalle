/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sdis_application;

import java.util.Scanner;

/**
 *
 * @author Pierre
 */
public class SDIS_Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.println("Charge a ajouter ?");
            int charge = sc.nextInt();
            int result=operation(charge);
            System.out.println("Nombre de worker = " + result);
        }
    }

    private static int operation(int charge) {
        org.me.post.PosterMessage_Service service = new org.me.post.PosterMessage_Service();
        org.me.post.PosterMessage port = service.getPosterMessagePort();
        return port.operation(charge);
    }

 

    
}
