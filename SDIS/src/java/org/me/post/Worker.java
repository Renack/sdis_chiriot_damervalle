package org.me.post;

import java.io.IOException;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.ShutdownSignalException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Worker extends Thread{

	private static final String TASK_QUEUE_NAME = "coucou_queue";
	private static final String HOST_NAME = "ec2-54-73-194-42.eu-west-1.compute.amazonaws.com";
        
        private QosManager manager;
        
        public Worker(QosManager qos){
            this.manager=qos;
        }

        private boolean alive = true;
        
        @Override
        public void run(){
            try {
                ConnectionFactory factory = new ConnectionFactory();
                factory.setHost(HOST_NAME);
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel();
                
                channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
                System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
                
                channel.basicQos(1);
                
                QueueingConsumer consumer = new QueueingConsumer(channel);
                channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
                
                while (alive) {
                    QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                    String message = new String(delivery.getBody());
                    
                    System.out.println(" [x] Received '" + message + "'");
                    doWork(message);
                    System.out.println(" [x] Done" );
                    
                    channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
                }
                this.manager.removeWorker(this);
            } catch (IOException ex) {
                Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ShutdownSignalException ex) {
                Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ConsumerCancelledException ex) {
                Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
	private void doWork(String message){
            try {
                int charge = Integer.parseInt(message);
                this.sleep(charge*1000);
                manager.changeCharge(-charge);
            } catch (InterruptedException ex) {
                Logger.getLogger(Worker.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        public void kill(){
            this.alive=false;
        }
        

        
}