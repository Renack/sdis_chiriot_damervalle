/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.post;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.jws.Oneway;



/**
 *
 * @author Pierre
 */
@WebService(serviceName = "PosterMessage")
@Stateless()
public class PosterMessage {

    private static final String TASK_QUEUE_NAME = "coucou_queue";
    private static final String HOST_NAME = "ec2-54-73-194-42.eu-west-1.compute.amazonaws.com";
    
    private QosManager qos = new QosManager();
    /**
     * Web service operation
     */
    @WebMethod(operationName = "operation")
    public int operation(@WebParam(name = "charge") int charge) {
        try {
            qos.changeCharge(charge);
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(HOST_NAME);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
            
            String message = ""+charge;
            
            channel.basicPublish( "", TASK_QUEUE_NAME,
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    message.getBytes());
            
            channel.close();
            connection.close();
        } catch (IOException ex) {
            Logger.getLogger(PosterMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
        return qos.getNbWorker()+1;

    }
}
