/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.me.post;


import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Pierre
 */
public class QosManager {
    private static final String TASK_QUEUE_NAME = "coucou_queue";
    private static final String HOST_NAME = "ec2-54-73-194-42.eu-west-1.compute.amazonaws.com";
    
    private List<Worker> worker = new ArrayList<Worker>();
    private int charge=0;
    private int seuil_max=60;
        
    public void addWorker(){
        Worker w = new Worker(this);
        w.start();
        this.worker.add(w);
    }
    
    public void deleteWorker(){
        Worker w = this.worker.get(0);
        w.kill();
    }
    
    public void changeCharge(int charge){
        this.charge+=charge;
        if((this.charge>0 && this.worker.isEmpty() )|| this.charge>seuil_max){
            this.addWorker();
        }
        if(this.charge<seuil_max){
            this.deleteWorker();
        }    
    }
    
    public int getNbWorker(){
        return this.worker.size();
    }
    
    public void removeWorker(Worker w){
        this.worker.remove(w);
    }
}
